import json
from msilib import datasizemask
from xml.etree.ElementTree import tostring
from flask import Flask, abort, g, jsonify, make_response, request, Blueprint
import pandas as pd

app = Flask(__name__)

@app.route('/')
def index() :
    return {"status":True}



@app.route('/example/<id>')
def get_id(id) :

    if id == "id" :
        abort(404)

    user_agent = request.headers.get('User-Agent')
    print(request.content_length)
    res = {
        "status": True,
        "id": id,
        "userAgent":user_agent
    }
    
    response = make_response(res)
    response.set_cookie('answer', '42')
    return response



ALLOWED_EXTENSIONS = {'xlsx', 'csv'}


def allowed_file(filename):
    return {"allowed": '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS, "extension":filename.rsplit('.', 1)[1]} 

@app.route('/upload', methods=['POST'])
def upload_file():
    
    try :
        file = request.files.get('file')
        print(allowed_file(file.filename)['extension'])
        if allowed_file(file.filename) :
            # DO read file using Pandas
            d = pd.read_csv(file)
            return {
                "success":True,
                "extension" : allowed_file(file.filename)['extension'],
                "data": {"title":[a for a in d['title']]}
            }
    except Exception as e:
        print('Error')
        return {"success":False, "error":e.args}
        


if __name__ == '__main__' :
    app.run(debug=True)